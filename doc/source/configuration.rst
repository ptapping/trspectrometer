.. _configuration:

Configuration
=============

.. note::

  Configuration is only necessary for systems which require data acquisition capabilities. To use
  the software in a visualisation/analysis mode only, this section is likely irrelevant and can be
  skipped.


.. _configuration file:

Configuration File
^^^^^^^^^^^^^^^^^^

The configuration file is named ``trspectrometer.toml`` and is stored in a user configuration
directory which is operating system specific. On application start, the location of the
configuration file is displayed in the log (View->Log). For Linux operating systems, this should be
``/home/$USER/.config/trspectrometer``, and
``C:\Users\$USER\AppData\Local\trspectrometer`` on Windows (where ``$USER`` is your username).

The configuration file format is `TOML <https://github.com/toml-lang/toml>`_, which is much like the
Windows ``.ini`` format. Currently, there is no application configuration window in the GUI so,
while the application defaults probably don't need modification, editing the configuration file
manually is the only way to customise the application. The configuration file should have sufficient
comments to explain each entry. If you happen to break the application by setting an invalid entry,
you may delete entries or sections at will and the defaults will be recreated.

.. note::

    The configuration file will be overwritten on application exit.
    If editing the configuration file manually, first close the application.


.. _plugins:

Plugins
^^^^^^^

The application uses a plugin module system which allows the behaviour of the program to be varied.
This has a number of advantages:

- Data collection requires specialised hardware to be attached to the computer. There are many types
  and brands of equipment which can perform similar jobs, so a different piece of hardware can be
  supported by substituting a hardware driver plugin, without modification of the core application
  code.

- A user may wish to view and analyse data on a different computer to that which acquires the data.
  By omitting the hardware plugins, the application behaves as purely a data viewer.

- Functionality can be extended or modified without overwriting or breaking the original behaviour.
  A new plugin module can be derived from an existing one and substituted with just a configuration
  file change.

The plugin modules are simply python modules which are placed in a specific location and then
instructed to be loaded via a line in the :ref:`configuration file`. See the :ref:`user plugins`
section in the developer guide for more information about creating a plugin, and the API
Documentation under the :data:`~trspectrometer.plugins` package to see a listing of plugins included
with the application.


Plugin Locations
----------------

Some plugins are provided with the application, and found under the :data:`~trspectrometer.plugins`
package. User plugins can be placed in an arbitrary location, as long as the directory is added to
the ``plugins`` list in the ``directories`` section of the :ref:`configuration file`.

.. code-block:: toml

    [directories]
    # List of directories containing user plugin modules
    plugins = ["/home/user/Documents/TRPlugins"]


Loading Plugins
---------------

To load a plugin module, include its name in the ``load`` list in the ``plugins`` section of the
:ref:`configuration file`.

For example, a system configured for data acquisition will need to load a variety of hardware
plugins:

.. code-block:: toml

    [plugins]
    # List of plugin modules to load
    load = ["aligncam", "delay", "chopper", "detector", "interface", "acquisition", "scope"]

On the other hand, a system which only needs to be able to display previously collected data may
need no plugins at all, with an empty ``load = []`` line.


Command Line Parameters
^^^^^^^^^^^^^^^^^^^^^^^

Passing the ``--help`` command line parameter to the application will show the available command
line parameters:

.. code-block:: none

    usage: __main__.py [-h] [--hardware {true,false}] [--loglevel {critical,error,warning,info,debug}]

    Run the TRSpectrometer application.

    optional arguments:
    -h, --help              show this help message and exit
    --hardware {true,false}
                            Perform hardware detection and initialisation.
    --loglevel {critical,error,warning,info,debug}
                            Verbosity of the log output.

The following command line parameters are accepted which change the behaviour of the application:

* ``--hardware``

    ``--hardware=true`` indicates that spectrometer hardware is attached, and the application will
    attempt to detect and initialise it. ``--hardware=false`` indicates that spectrometer hardware
    is not attached, and the application should run in data visualisation/analysis mode only.

    The effect of this switch is permanent, in that if ``--hardware=true`` is specified, it is
    assumed that hardware will be connected in the future and should also be detected on subsequent
    application start.

* ``--loglevel``

    Set the verbosity of the logging output. The options are ``critical``, ``error``,
    ``warning``, ``info``, ``debug``, where critical produces the least amount of output, and
    debug produces the most.
