trspectrometer.plugins.detector package
=======================================

.. automodule:: trspectrometer.plugins.detector
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   trspectrometer.plugins.detector.dummydetector
   trspectrometer.plugins.detector.zylafixed
