trspectrometer.plugins.delay package
====================================

.. automodule:: trspectrometer.plugins.delay
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   trspectrometer.plugins.delay.dummydelay
   trspectrometer.plugins.delay.thorlabs_apt
