trspectrometer.plugins.acquisition package
==========================================

.. automodule:: trspectrometer.plugins.acquisition
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   trspectrometer.plugins.acquisition.ta_dummy
   trspectrometer.plugins.acquisition.ta_stepped
   trspectrometer.plugins.acquisition.ta_swept
