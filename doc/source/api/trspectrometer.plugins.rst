trspectrometer.plugins package
==============================

.. automodule:: trspectrometer.plugins
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   trspectrometer.plugins.acquisition
   trspectrometer.plugins.aligncam
   trspectrometer.plugins.chopper
   trspectrometer.plugins.delay
   trspectrometer.plugins.detector
   trspectrometer.plugins.interface
   trspectrometer.plugins.repratemenu
   trspectrometer.plugins.scope
   trspectrometer.plugins.zylaalign
