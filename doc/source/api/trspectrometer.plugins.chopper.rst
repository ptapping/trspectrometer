trspectrometer.plugins.chopper package
======================================

.. automodule:: trspectrometer.plugins.chopper
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   trspectrometer.plugins.chopper.dummychopper
   trspectrometer.plugins.chopper.thorlabs_mc2000b
