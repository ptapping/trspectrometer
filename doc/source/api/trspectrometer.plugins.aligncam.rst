trspectrometer.plugins.aligncam package
=======================================

.. automodule:: trspectrometer.plugins.aligncam
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   trspectrometer.plugins.aligncam.alignmentpanel
   trspectrometer.plugins.aligncam.cv2camera
