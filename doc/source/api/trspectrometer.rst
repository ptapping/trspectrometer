trspectrometer package
======================

.. automodule:: trspectrometer
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   trspectrometer.plugins

Submodules
----------

.. toctree::
   :maxdepth: 4

   trspectrometer.aboutpanel
   trspectrometer.busydialog
   trspectrometer.configuration
   trspectrometer.datapanel
   trspectrometer.datastorage
   trspectrometer.hardware
   trspectrometer.logpanel
   trspectrometer.mainwindow
   trspectrometer.qtwidgets
   trspectrometer.signalstorage
   trspectrometer.ufsfile
   trspectrometer.utils
