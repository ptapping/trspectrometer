trspectrometer.plugins.chopper.thorlabs\_mc2000b module
=======================================================

.. automodule:: trspectrometer.plugins.chopper.thorlabs_mc2000b
   :members:
   :undoc-members:
   :show-inheritance:
