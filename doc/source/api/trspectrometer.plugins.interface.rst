trspectrometer.plugins.interface package
========================================

.. automodule:: trspectrometer.plugins.interface
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   trspectrometer.plugins.interface.dummyinterface
   trspectrometer.plugins.interface.trsi
