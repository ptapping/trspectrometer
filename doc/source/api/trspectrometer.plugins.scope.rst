trspectrometer.plugins.scope package
====================================

.. automodule:: trspectrometer.plugins.scope
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   trspectrometer.plugins.scope.scopepanel
