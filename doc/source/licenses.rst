.. _license:

Licenses
========

All original work is free and open source, licensed under the GNU Public License. See the
`LICENSE.txt <https://gitlab.com/ptapping/trspectrometer/-/blob/main/LICENSE.txt>`__ for details.

Additional libraries may be downloaded and installed during installation. These may vary depending
on the plugins which are installed and loaded. The license agreements for the respective packages
remain independent from the main TRSpectrometer software.
