Getting Started
===============

Prerequisites
^^^^^^^^^^^^^

TRSpectrometer is written in Python, and thus requires a Python installation. If you don't already
have Python installed, it can be downloaded from the `Python website
<https://www.python.org/downloads/>`__. On Windows and MacOS, a popular Python distribution which
also includes many useful packages is `Anaconda <https://www.anaconda.com/products/individual>`__.
On Linux, you will most likely already have a Python installation, but if not, it should be easily
installed using your distribution's package manager. For example:

On Ubuntu and other Debian derivatives:
 
.. code-block:: sh
    
    sudo apt update && sudo apt install python3 python3-pip 

On Arch Linux:

.. code-block:: sh

    sudo pacman -Sy python python-pip

.. note::

    Python2 is end-of-life and no longer in development. This software only supports Python3, and
    thus all instruction on this page assume that ``python`` and ``pip`` refer to the version 3
    commands. If your Linux distribution's ``python`` command defaults to python2 (e.g. Ubuntu),
    substitute ``python3`` and ``pip3`` in commands where appropriate.


Optional Libraries
------------------

The `numba <https://numba.pydata.org/>`__ library is used by some components of the application if
available. While it can boost performance, it may not be available on all platforms. You can attempt
to install it with:

.. code-block:: sh

    pip install --user --upgrade numba

If the install fails, it may be because your system architecture or python version does not have a
pre-compiled numba package in "wheel" format. Compiling from the "sdist" source package, or from the
`source code <https://numba.readthedocs.io/en/stable/user/installing.html#installing-from-source>`__
should work, but requires a system install of llvm. See the `install instructions for llvmlite
<http://llvmlite.pydata.org/en/latest/admin-guide/install.html>`__ for more information.


Installing the Software
^^^^^^^^^^^^^^^^^^^^^^^

Download Using Pip
------------------

The package installer for Python (pip) is the typical method for installing a package. This method
will install the latest "release" version from the `Python Package Index
<https://pypi.org/project/trspectrometer/>`__.

.. code-block:: sh

    pip install --user --upgrade trspectrometer

The ``--user`` parameter installs using user-level permissions, thus does not require root or
administrator privileges. To install system wide, remove the ``--user`` parameter and prefix the
command with ``sudo`` (Linux, MacOS), or run as administrator (Windows).

Clone From Git
--------------

Alternatively, the latest version can be downloaded from the git repository:

.. code-block:: sh

    git clone https://gitlab.com/ptapping/trspectrometer.git

Then installed to your system using ``pip``:

.. code-block:: sh

    pip install --user ./trspectrometer


Starting
^^^^^^^^

On Linux, a TRSpectrometer shortcut should have be added to your application menus. Sorry Windows
and MacOS users, this functionality hasn't been implemented for your OS yet (see issues `here
<https://gitlab.com/ptapping/trspectrometer/-/issues/1>`__ and `here
<https://gitlab.com/ptapping/trspectrometer/-/issues/2>`__). Pull requests to add this functionality
are welcome!

The application can be run from the command line:

.. code-block:: sh

    trspectrometer

The default configuration uses a set of "dummy" hardware, which simulate the presence of real
devices. These allow some of the data acquisition features of the application to be demonstrated and
tested without requiring real hardware to be connected and configured. Feel free to dive straight in
by hitting the ``Start`` button at the lower right on the ``Data`` tab. While your dummy data is
being collected, you may wish to read about :ref:`acquisition parameters`, and :ref:`viewing data`.

To actually acquire real data, the appropriate :ref:`hardware` will need to be configured.
