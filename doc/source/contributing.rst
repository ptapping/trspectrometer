.. _contributing:

Contributing
============

This is an open-source project which means that it will only reach its true potential via community
contributions from users and other interested parties. Contributions can take many forms, you don't
need to be a software wizard, electronic engineer, or expert experimentalist to help!


Report Bugs, Suggest Changes or Features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Use the issue tracker at `<https://gitlab.com/ptapping/trspectrometer/-/issues>`__ to report bugs,
suggest changes to behaviour, or describe new ideas for desirable features.


Fork, Make a Merge Request
^^^^^^^^^^^^^^^^^^^^^^^^^^

From the source code repository at `<https://gitlab.com/ptapping/trspectrometer>`__, the software
can be forked to create your own custom version. If you make any changes which you think others may
find useful, make a merge request to get them integrated into the main code base. Changes can be as
simple as fixing a spelling mistake in the documentation, to adding support for a specific piece of
hardware, or adding a whole new method of experimental data acquisition.


Publicise
^^^^^^^^^

Tell others about this project, and refer to it in your publications.


Fund
^^^^

This platform contains both free software and hardware designs. Note that that means `"free, as in
speech", which is different to "free, as in beer"
<https://www.gnu.org/philosophy/free-sw.en.html>`__. If you would like a particular feature or
support for some brand of hardware, this may be implemented through the goodness of someone's heart,
but there should be no expectation that anyone will do as you say, even if you ask nicely. Consider
funding a developer to implement the feature you desire. This can be a private arrangement, with the
changes kept private if you so desire, provided the terms of the relevant :ref:`license agreements
<license>` are upheld.
