Welcome to TRSpectrometer's documentation!
==========================================

.. image:: images/trspectrometer-title.png

TRSpectrometer is a platform for time-resolved spectroscopy, encompassing both software and hardware
designs. It can be used for both data acquisition, as well as viewing data and basic analysis.

Features:

 - Completely `open source <https://gitlab.com/ptapping/trspectrometer>`__ hardware and software
   designs. Don't get stuck with inflexible and buggy proprietary software!

 - :ref:`Plugin architecture <plugins>` to ease modifications and integration of new hardware
   devices or experimental methods.
 
 - Supports the latest :data:`rapid-acquisition <trspectrometer.plugins.acquisition.ta_swept>`
   techniques.

 - Cross platform python - Linux, Windows, MacOS.

The aim of the software is to be as open and flexible as possible, using a plugin system for the
addition and customisation of features and functionality. For example, the same software that runs
on a Windows machine acquiring data can be installed on a Linux or Mac OS computer and used for data
viewing and analysis without restriction. The majority of the code base is written in Python, making
development and modifications (hopefully!) more accessible to students and researchers who lack
specific programming backgrounds.

The initial 1.0 version includes a reference hardware design for a transient absorption
(pump--probe) spectrometer and a cross-platform software application for data acquisition and basic
data exploration and analysis. In time, it is hoped that the platform will support a wider variety
of hardware and additional time-resolved experimental techniques, as well as adding more advanced
data exploration, analysis, and plotting tools. This will only truly be possible through
contributions by users and other interested parties. See the :ref:`contributing` page for more
information.

Source code at `<https://gitlab.com/ptapping/trspectrometer>`__.

Documentation online at `<https://ptapping.gitlab.io/trspectrometer>`__.

Python Packaging Index (PyPI) page at `<https://pypi.org/project/trspectrometer>`__.

Bug reports, feature requests and suggestions can be submitted to the `issue tracker
<https://gitlab.com/ptapping/trspectrometer/-/issues>`__.


User Guide
----------

.. toctree::
  :maxdepth: 1

  gettingstarted
  hardware
  configuration
  acquisition
  viewing
  dataformat
  versions
  contributing
  licenses


Developer Guide
---------------

These sections will be useful for those wishing to create plugins or understand the design of the
software. They are not required to be understood by the typical end user!

.. toctree::
  :maxdepth: 1

  development

API Documentation
`````````````````

.. toctree::
   :maxdepth: 5
   :titlesonly:

   api/trspectrometer


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
