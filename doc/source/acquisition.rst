Acquiring Data
==============

.. note:: Data acquisition requires the appropriate :ref:`hardware` to be installed and :ref:`configured <configuration>`.

At this point you should have a sample prepared and the appropriate excitation (pump) laser
wavelength available. Before hitting the ``Start`` button, the :ref:`alignment` of the instrument
should be checked and suitable :ref:`acquisition parameters` decided upon. 

.. toctree::
    :maxdepth: 2

    alignment
    acquisition_params
