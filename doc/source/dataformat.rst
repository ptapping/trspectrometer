Native Data Format
==================

The application's native data format is stored on disk as a directory tree, which uses the python
`zarr <https://zarr.readthedocs.io>`__ structure. This is a sharded, compressed, binary format which
makes it fast and space-efficient. Data directories can be identified by their ``.tr.zarr`` suffix.
Accessing the information in this form is straightforward in Python.

Version 1.0 of the data format is designed to store only raw data and associated metadata. A
tutorial-style python notebook is provided to demonstrate how to access data for analysis.


.. toctree::
   :maxdepth: 2
   :caption: Example Notebook:

   notebooks/demo_trdata