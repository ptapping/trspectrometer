<!DOCTYPE html>

<html lang="en" data-content_root="./">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Alignment &#8212; TRSpectrometer 1.2.4 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css?v=d75fae25" />
    <link rel="stylesheet" type="text/css" href="_static/sphinxdoc.css?v=34905f61" />
    <link rel="stylesheet" type="text/css" href="_static/mystnb.4510f1fc1dee50b3e5859aac5469c37c29e427902b24a333a5f9fcb2f0b3ac41.css" />
    <link rel="stylesheet" type="text/css" href="_static/css/custom.css?v=b64a1d6a" />
    <script src="_static/documentation_options.js?v=928db92d"></script>
    <script src="_static/doctools.js?v=9bcbadda"></script>
    <script src="_static/sphinx_highlight.js?v=dc90522c"></script>
    <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Configuring Acquisition Parameters" href="acquisition_params.html" />
    <link rel="prev" title="Acquiring Data" href="acquisition.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="Related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="acquisition_params.html" title="Configuring Acquisition Parameters"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="acquisition.html" title="Acquiring Data"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">TRSpectrometer 1.2.4 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="acquisition.html" accesskey="U">Acquiring Data</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Alignment</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="alignment">
<span id="id1"></span><h1>Alignment<a class="headerlink" href="#alignment" title="Link to this heading">¶</a></h1>
<p>The alignment of the spectrometer should be checked before acquiring any data.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>The part references used in this section refer to components in the <a class="reference internal" href="hardware.html#reference-design"><span class="std std-ref">Transient Absorption Spectrometer Reference Design</span></a>.
Components and layout can vary widely between different time-resolved spectrometers, but many
parts will have equivalents and the fundamental steps are likely still be relevant.</p>
</div>
<section id="white-light-continuum-probe">
<h2>White-light Continuum (Probe)<a class="headerlink" href="#white-light-continuum-probe" title="Link to this heading">¶</a></h2>
<p>The quality of the white light is critical to obtaining good data. It is worth spending some time
ensuring that the probe is stable and as high quality as possible.</p>
<section id="delay-line-passes">
<h3>Delay Line Passes<a class="headerlink" href="#delay-line-passes" title="Link to this heading">¶</a></h3>
<p>The aim of this step is to ensure the seed beam for the probe enters and exits the delay line along
a straight path with no deviation as the delay is moved along its range. To help with this, the
<a class="reference internal" href="api/trspectrometer.plugins.aligncam.html#module-trspectrometer.plugins.aligncam" title="trspectrometer.plugins.aligncam"><code class="xref py py-data docutils literal notranslate"><span class="pre">aligncam</span></code></a> <a class="reference internal" href="configuration.html#plugins"><span class="std std-ref">plugin module</span></a> should be loaded, and an
appropriate alignment camera (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">AC</span></a>) connected.</p>
<p>Select the <code class="docutils literal notranslate"><span class="pre">Align</span></code> tab on the main window. You should now be viewing the seed laser spot as
seen on a screen by the alignment webcam.</p>
<figure class="align-default" id="id2">
<img alt="Alignment panel of the TRSpectrometer software." src="_images/alignmode-bad.png" />
<figcaption>
<p><span class="caption-text">Alignment panel indicating poor alignment through the delay line. The blue and red ellipses
should be overlapping when there is no beam deviation due to the delay movement.</span><a class="headerlink" href="#id2" title="Link to this image">¶</a></p>
</figcaption>
</figure>
<p>If multiple cameras are connected, select the correct one using the <code class="docutils literal notranslate"><span class="pre">Camera</span></code> combo box. If a
camera has been disconnected or reconnected while the application is running, try using the
<code class="docutils literal notranslate"><span class="pre">Reset</span></code> button to reinitialise the cameras.</p>
<p>The blue ellipse on the plot tracks the position of the spot when the <code class="docutils literal notranslate"><span class="pre">Delay</span> <span class="pre">Position</span></code> slider is
in the blue <code class="docutils literal notranslate"><span class="pre">Start</span></code> position, the red ellipse tracks the position when the slider is in the red
<code class="docutils literal notranslate"><span class="pre">End</span></code> position. When the <code class="docutils literal notranslate"><span class="pre">Auto</span> <span class="pre">tracking</span></code> checkbox is checked, the positions will be updated
automatically using a computer vision algorithm. In manual mode, the ellipses can be moved, resized
and reshaped using the handles. If a delay is attached and functioning, changing the <code class="docutils literal notranslate"><span class="pre">Delay</span>
<span class="pre">Position</span></code> slider will also cause the delay to move to the start and end of its extremes. If a delay
is not connected or not functional, the laser spot tracking will still switch between the two
ellipses. Depending on the specific hardware, you may still be able to move the delay manually to
see the effects.</p>
<p>Using the <code class="docutils literal notranslate"><span class="pre">Delay</span> <span class="pre">Position</span></code> slider, run the delay to the start position, and then to the end
position. The blue ellipse will now designate where the laser spot was at the start of the delay,
and the red ellipse will be currently tracking the position of the spot (at the end of the delay).
The statistics in the lower-right corner will indicate how the laser spot has changed over the
length of the delay. We ideally want to the two spots to be identical (no translation, 1.0
divergence factor, 1.0 aspect ratio change, no rotation).</p>
<p>In the above image, there is significant deviation which needs to be corrected. The basic procedure is:</p>
<ul class="simple">
<li><p>Adjust the mirror directing the seed into the delay line (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">M1</span></a>), and observe the
effect on the screen. To begin with, try moving the mirror so that the red ellipse is on top of
the blue ellipse.</p></li>
<li><p>Move the delay position to the start, and then back to the end.</p></li>
<li><p>Note if your mirror adjustment improved things. If so, repeat the above steps until the two ellipses
are co-incident at both the start and end positions. You may find you need to “overshoot” the
adjustment a fraction to finish the last bit of alignment. If your adjustment made things worse,
try again, perhaps only adjusting the horizontal or vertical deflection until you figure out
what’s going on.</p></li>
</ul>
<p>It is likely you won’t ever be able to get the alignment perfect, but get it as good as you can. If
you’re having problems, check that the beam is not getting clipped off at the edge of any mirrors or
similar.</p>
<figure class="align-default" id="id3">
<img alt="Alignment panel of the TRSpectrometer software." src="_images/alignmode-good.png" />
<figcaption>
<p><span class="caption-text">Alignment panel after correct adjustment of beam through the delay line.</span><a class="headerlink" href="#id3" title="Link to this image">¶</a></p>
</figcaption>
</figure>
</section>
<section id="continuum-generation">
<h3>Continuum Generation<a class="headerlink" href="#continuum-generation" title="Link to this heading">¶</a></h3>
<p>Check that a continuum is being generated in the white-light generation medium (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">WLG</span></a>)
using a white card. The light should be a single, stable spot. Adjust the position of the medium
using the micrometer stage and intensity of the seed beam using the wave plate (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">WP1</span></a>)
if necessary.</p>
<p>Pulling a good continuum can be a little bit of a black art, so some trial-and-error may be needed.
For a sapphire WLG medium, the best stability tends to be found using an intensity somewhat high
relative to where the continuum is first established. For a deuterium oxide (D<sub>2</sub>O,
heavy water) medium however, the optimal intensity is only a little higher than where the continuum
is first seen. An excessive intensity and/or high laser repetition rates will cause bubbles to form
which will destroy the continuum generation.</p>
<figure class="align-default" id="id4">
<img alt="Photographs of white-light continuum generation using different seed intensities." src="_images/white-light.jpg" />
<figcaption>
<p><span class="caption-text">Photographs of white light continua seen on a white business card, produced using different
intensities of the seed laser. The WLG medium was a 12 mm sapphire crystal. The seed was 1030 nm,
with approximately 160 fs pulse duration and 1 µJ pulse energy, focussed onto the medium using a f
= 300 mm lens. In (a), the pulse energy is just high enough to produce light, but will not be
useful as a probe. The continuum in (b) looks almost perfect, but using a slightly higher
intensity (c) may give better results. When too high intensity is used, the continuum will start
to break up and produce additional filaments, such as in (d).</span><a class="headerlink" href="#id4" title="Link to this image">¶</a></p>
</figcaption>
</figure>
</section>
<section id="focus-on-the-sample">
<h3>Focus on the Sample<a class="headerlink" href="#focus-on-the-sample" title="Link to this heading">¶</a></h3>
<p>Immediately after the white-light generation (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">WLG</span></a>) medium is a concave mirror
(<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">CM1</span></a>) which collects the diverging continuum and begins focussing it down on the
sample. Adjust the position of this mirror using its micrometer stage so that the size of the spot
at the sample position is as small as possible.</p>
<p>Placing a piece of black anodised aluminium at the sample position will help with determining the
size of the spot by eye. If your sample is in a cuvette, note that the sample position will actually
be a millimeter or so behind the front face of the cuvette.</p>
<p>Open up the iris (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">I1</span></a>) immediately after the sample, and then check that the white
light is centred on the target iris (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">I2</span></a>) just before the detector. Adjust the
concave (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">CM1</span></a>) and/or steering (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">M5</span></a>) mirrors if necessary. If a large
adjustment is made, go back and check that the focus at the sample position is still as tight as
possible. Open up I2 completely when alignment is complete.</p>
<p>Ensure the white light passes cleanly through any filters (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">F1</span></a>) which may be in place
between the WLG and the sample. Finally, close down the iris (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">I1</span></a>) directly after the
sample. If the white light does not pass cleanly through this, adjust the position of the iris (this
iris is to block the residual pump, it is not an alignment target!).</p>
</section>
<section id="aim-into-detector">
<h3>Aim into Detector<a class="headerlink" href="#aim-into-detector" title="Link to this heading">¶</a></h3>
<p>The <code class="docutils literal notranslate"><span class="pre">Scope</span></code> panel allows viewing of the raw detector data to assist with this alignment step.
The <a class="reference internal" href="api/trspectrometer.plugins.scope.html#module-trspectrometer.plugins.scope" title="trspectrometer.plugins.scope"><code class="xref py py-data docutils literal notranslate"><span class="pre">scope</span></code></a> <a class="reference internal" href="configuration.html#plugins"><span class="std std-ref">plugin module</span></a> needs to be loaded for the
panel to appear.</p>
<p>Select the <code class="docutils literal notranslate"><span class="pre">Scope</span></code> tab on the main window, and ensure that <code class="docutils literal notranslate"><span class="pre">Raw</span></code> mode is selected. The raw
spectrum being detected by the detector hardware will be displayed in the plot area.</p>
<figure class="align-default" id="id5">
<img alt="Scope panel of the TRSpectrometer software." src="_images/scope-raw.png" />
<figcaption>
<p><span class="caption-text">Scope panel showing the raw white light spectrum being detected by the detector.</span><a class="headerlink" href="#id5" title="Link to this image">¶</a></p>
</figcaption>
</figure>
<p>Adjust the steering mirror (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">M7</span></a>) before the detector to maximise the intensity and
bandwidth of the displayed spectrum. The position of the focussing lens (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">L2</span></a>) may also
need optimising if the character or alignment of the white light has changed significantly.</p>
</section>
<section id="check-delayed-spectrum">
<h3>Check Delayed Spectrum<a class="headerlink" href="#check-delayed-spectrum" title="Link to this heading">¶</a></h3>
<p>The character of the white light can change over the course of the delay due to deviation or
divergence of the seed beam. In the extreme case, the continuum can completely fail from one end of
the delay to the other. This should be checked and the white light generation adjusted to minimise
this effect if required.</p>
<p>The scope panel has some features to assist with this step. Use the delay <span class="math notranslate nohighlight">\(|\lt\)</span> and
<span class="math notranslate nohighlight">\(\gt|\)</span> controls to run the delay through to one extreme. Click the pin button to temporarily
pin the current spectrum to the plot, then run the delay to the opposite extreme. Any changes in the
spectrum should become obvious.</p>
<figure class="align-default" id="id6">
<img alt="Scope panel of the TRSpectrometer software, with pinned spectrum." src="_images/scope-raw-pinned.png" />
<figcaption>
<p><span class="caption-text">Scope panel showing a “pinned” spectrum (in red) used to check the stability of the continuum over
the course of the delay. There is a small but acceptable change in the character of the spectrum
between the start and end of the delay range.</span><a class="headerlink" href="#id6" title="Link to this image">¶</a></p>
</figcaption>
</figure>
<p>If there is an unacceptable amount of change in the spectrum, it may be necessary to repeat some or
all of the alignment steps listed in this section, for example, by checking the delay line passes
again.</p>
</section>
<section id="check-signal-quality">
<h3>Check Signal Quality<a class="headerlink" href="#check-signal-quality" title="Link to this heading">¶</a></h3>
<p>Note that air currents and table vibration can have a significant effect on the stability of the
white light. To properly evaluate the character of the continuum, the spectrometer lid should be on
and a few seconds provided for things to settle.</p>
<p>Even when the raw spectrum appears stable, there can be momentary glitches and dropped pulses which
can cause severe degradation of the acquired signal. It is worth checking the change in absorbance
(ΔA) signal to ensure it looks good too.</p>
<p>Switch the scope to <code class="docutils literal notranslate"><span class="pre">ΔA</span></code> mode. There is no need for a sample or pump light to be present at this
stage. With no sample and/or no pump light, the signal should theoretically be zero. Of course, with
detector noise and laser fluctuations this will never be the case. At this point, what is considered
“good” is somewhat subjective and dependent on several factors such as the WLG medium type. If
excessive or extreme fluctuations are observed, try adjusting the intensity of the white light
slightly. Note that if the intensity is changed, the later alignment steps should also be checked.</p>
<figure class="align-default" id="id7">
<img alt="Scope panel of the TRSpectrometer software, with a zero delta A signal displayed." src="_images/scope-da-nothing.png" />
<figcaption>
<p><span class="caption-text">Scope panel showing a “blank” signal, with no sample or pump laser present. Ideally, the signal
should be a flat, stable zero across the spectrum. The increased noise at the blue end of the
spectrum is expected due to a low intensity of the white light probe in that region.</span><a class="headerlink" href="#id7" title="Link to this image">¶</a></p>
</figcaption>
</figure>
</section>
<section id="saving-spectrum-for-later-reference">
<h3>Saving Spectrum for Later Reference<a class="headerlink" href="#saving-spectrum-for-later-reference" title="Link to this heading">¶</a></h3>
<p>Once an acceptable continuum is achieved, the spectrum can be saved for later reference. Switch the
scope to <code class="docutils literal notranslate"><span class="pre">Raw</span></code> mode, then click the <code class="docutils literal notranslate"><span class="pre">Save</span></code> button. Type a filename (or accept the default
suggestion).</p>
<p>Use the <code class="docutils literal notranslate"><span class="pre">Load</span></code> button to load a previously saved spectrum. The loaded spectrum will persist on the
plot until the <code class="docutils literal notranslate"><span class="pre">Unload</span></code> button is pressed (even between application restarts). With a loaded
spectrum for reference, it is easy to check if the alignment has drifted over the course of time.</p>
<figure class="align-default" id="id8">
<img alt="Scope panel of the TRSpectrometer software, with loaded spectrum." src="_images/scope-raw-loaded.png" />
<figcaption>
<p><span class="caption-text">Scope panel showing a loaded spectrum (in grey) which remains on the plot for reference purposes.</span><a class="headerlink" href="#id8" title="Link to this image">¶</a></p>
</figcaption>
</figure>
</section>
</section>
<section id="excitation-pump">
<h2>Excitation (Pump)<a class="headerlink" href="#excitation-pump" title="Link to this heading">¶</a></h2>
<p>Assuming that the pump laser is suitable for the experiment (appropriate wavelength, pulse
duration), the alignment through the spectrometer is relatively straightforward. It simply needs
to pass along the appropriate path, through the chopper wheel and onto the sample.</p>
<section id="beam-paths">
<h3>Beam Paths<a class="headerlink" href="#beam-paths" title="Link to this heading">¶</a></h3>
<ul class="simple">
<li><p>Using the external steering mirror, direct the beam onto <a class="reference internal" href="hardware.html#taref"><span class="std std-ref">M8</span></a> and through the centre
of the iris target (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">I3</span></a>).</p></li>
<li><p>Use <a class="reference internal" href="hardware.html#taref"><span class="std std-ref">M8</span></a> to aim through the waveplate (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">WP2</span></a>) and polariser (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">P2</span></a>) onto <a class="reference internal" href="hardware.html#taref"><span class="std std-ref">M9</span></a> and iris target <a class="reference internal" href="hardware.html#taref"><span class="std std-ref">I4</span></a>.</p></li>
<li><p>Use <a class="reference internal" href="hardware.html#taref"><span class="std std-ref">M9</span></a> to aim through the lens (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">L5</span></a>) and chopper wheel (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">CW</span></a>) onto iris target <a class="reference internal" href="hardware.html#taref"><span class="std std-ref">I5</span></a>.</p></li>
<li><p>Repeat the above steps as needed until the beam passes cleanly though all the elements.</p></li>
<li><p>Use <a class="reference internal" href="hardware.html#taref"><span class="std std-ref">M10</span></a> to direct the pump beam so that it overlaps with the probe light at the
sample position.</p></li>
</ul>
</section>
<section id="spot-size-and-pulse-energy">
<h3>Spot Size and Pulse Energy<a class="headerlink" href="#spot-size-and-pulse-energy" title="Link to this heading">¶</a></h3>
<p>The position and/or focal length of the lens <a class="reference internal" href="hardware.html#taref"><span class="std std-ref">L3</span></a> can be adjusted to optimise the pump
spot size at the sample position. The pump should be approximately 3× the size of the probe. The
residual pump beam should hit the edge of the iris (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">I1</span></a>) and be blocked.</p>
<p>The pump power can be adjusted using the neutral density filter wheel (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">ND</span></a>) and/or the
waveplate (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">WP2</span></a>).</p>
</section>
<section id="pump-and-probe-overlap">
<h3>Pump and Probe Overlap<a class="headerlink" href="#pump-and-probe-overlap" title="Link to this heading">¶</a></h3>
<p>The pump beam needs to be properly overlapped with the probe at the sample position. This can be
optimised using the Scope panel in <code class="docutils literal notranslate"><span class="pre">ΔA</span></code> mode.</p>
<p>Place a sample into position. When using unknown or troublesome samples, using a test sample (eg. a
dye which absorbs the same pump wavelength) can be helpful. This also allows high pump powers to be
used without fear of damaging or degrading a real sample.</p>
<p>Switch to the <code class="docutils literal notranslate"><span class="pre">Scope</span></code> tab and select <code class="docutils literal notranslate"><span class="pre">ΔA</span></code> mode. Use the <code class="docutils literal notranslate"><span class="pre">Delay</span></code> controls to select a delay
time you know will be after time zero (the time when the pump and probe arrive simultaneously). If
your sample has a very short excited-state lifetime, then finding the correct time can be difficult
(again, an appropriate test sample can be useful here).</p>
<p>Adjust the steering mirror (<a class="reference internal" href="hardware.html#taref"><span class="std std-ref">M10</span></a>) until the pump-probe overlap is optimised. It may be
helpful at first to use a white card to trace the path of the beams onto the sample. Once a small
signal is observed on the plot, make small adjustments of <a class="reference internal" href="hardware.html#taref"><span class="std std-ref">M10</span></a> until the magnitude of
the signal is maximsed.</p>
<figure class="align-default" id="id9">
<img alt="Scope panel of the TRSpectrometer software, showing signal when pump probe overlap is optimised." src="_images/scope-da-overlap.png" />
<figcaption>
<p><span class="caption-text">Scope panel showing a change in absorbance (ΔA) signal which should be maximised when optimising
the pump probe beam overlap on the sample.</span><a class="headerlink" href="#id9" title="Link to this image">¶</a></p>
</figcaption>
</figure>
</section>
<section id="find-time-zero">
<h3>Find Time Zero<a class="headerlink" href="#find-time-zero" title="Link to this heading">¶</a></h3>
<p>Use the <code class="docutils literal notranslate"><span class="pre">Delay</span></code> controls to reduce the delay time until the signal disappears. This is now “before
time zero”. Make smaller increments upwards until the signal reappears, and repeat until you narrow
down the time when the signal just starts to appear. Make a note of the time, as that is the time
zero for the current configuration of the spectrometer.</p>
<p>The <code class="docutils literal notranslate"><span class="pre">&lt;</span></code> and <code class="docutils literal notranslate"><span class="pre">&gt;</span></code> buttons will jump by a small amount, while the up and down arrows of the spin
box will make fine adjustments. A time can also be typed in directly into the spin box (press enter
to commit the change).</p>
</section>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="Main">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">Alignment</a><ul>
<li><a class="reference internal" href="#white-light-continuum-probe">White-light Continuum (Probe)</a><ul>
<li><a class="reference internal" href="#delay-line-passes">Delay Line Passes</a></li>
<li><a class="reference internal" href="#continuum-generation">Continuum Generation</a></li>
<li><a class="reference internal" href="#focus-on-the-sample">Focus on the Sample</a></li>
<li><a class="reference internal" href="#aim-into-detector">Aim into Detector</a></li>
<li><a class="reference internal" href="#check-delayed-spectrum">Check Delayed Spectrum</a></li>
<li><a class="reference internal" href="#check-signal-quality">Check Signal Quality</a></li>
<li><a class="reference internal" href="#saving-spectrum-for-later-reference">Saving Spectrum for Later Reference</a></li>
</ul>
</li>
<li><a class="reference internal" href="#excitation-pump">Excitation (Pump)</a><ul>
<li><a class="reference internal" href="#beam-paths">Beam Paths</a></li>
<li><a class="reference internal" href="#spot-size-and-pulse-energy">Spot Size and Pulse Energy</a></li>
<li><a class="reference internal" href="#pump-and-probe-overlap">Pump and Probe Overlap</a></li>
<li><a class="reference internal" href="#find-time-zero">Find Time Zero</a></li>
</ul>
</li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="acquisition.html"
                          title="previous chapter">Acquiring Data</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="acquisition_params.html"
                          title="next chapter">Configuring Acquisition Parameters</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/alignment.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<search id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</search>
<script>document.getElementById('searchbox').style.display = "block"</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="Related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="acquisition_params.html" title="Configuring Acquisition Parameters"
             >next</a> |</li>
        <li class="right" >
          <a href="acquisition.html" title="Acquiring Data"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">TRSpectrometer 1.2.4 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="acquisition.html" >Acquiring Data</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Alignment</a></li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
    &#169; Copyright 2021, Patrick Tapping.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 8.1.3.
    </div>
  </body>
</html>