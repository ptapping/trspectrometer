<!DOCTYPE html>

<html lang="en" data-content_root="./">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Loading and Viewing Raw Data &#8212; TRSpectrometer 1.2.4 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css?v=d75fae25" />
    <link rel="stylesheet" type="text/css" href="_static/sphinxdoc.css?v=34905f61" />
    <link rel="stylesheet" type="text/css" href="_static/mystnb.4510f1fc1dee50b3e5859aac5469c37c29e427902b24a333a5f9fcb2f0b3ac41.css" />
    <link rel="stylesheet" type="text/css" href="_static/css/custom.css?v=b64a1d6a" />
    <script src="_static/documentation_options.js?v=928db92d"></script>
    <script src="_static/doctools.js?v=9bcbadda"></script>
    <script src="_static/sphinx_highlight.js?v=dc90522c"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Native Data Format" href="dataformat.html" />
    <link rel="prev" title="Configuring Acquisition Parameters" href="acquisition_params.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="Related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="dataformat.html" title="Native Data Format"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="acquisition_params.html" title="Configuring Acquisition Parameters"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">TRSpectrometer 1.2.4 documentation</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Loading and Viewing Raw Data</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="loading-and-viewing-raw-data">
<span id="viewing-data"></span><h1>Loading and Viewing Raw Data<a class="headerlink" href="#loading-and-viewing-raw-data" title="Link to this heading">¶</a></h1>
<section id="loading-raw-data">
<h2>Loading Raw Data<a class="headerlink" href="#loading-raw-data" title="Link to this heading">¶</a></h2>
<p>Data sets stored in the application’s native format can be opened using the <code class="docutils literal notranslate"><span class="pre">Dataset→Open...</span></code> menu
item. Note that the native format is actually a <a class="reference external" href="https://zarr.readthedocs.io">zarr</a> directory
tree, and not an individual file. These data directories can be identified by the <code class="docutils literal notranslate"><span class="pre">.tr.zarr</span></code>
suffix to their names.</p>
<p>Data stored in other formats can be imported using the <code class="docutils literal notranslate"><span class="pre">Dataset→Import→Raw</span> <span class="pre">Data...</span></code> menu item.
Currently, comma-separated values (<code class="docutils literal notranslate"><span class="pre">.csv</span></code>) files and the proprietary <code class="docutils literal notranslate"><span class="pre">.ufs</span></code> format from
Ultrafast Systems are supported. Multiple files may be loaded at once which represent multiple scans
of the same sample for averaging.</p>
<p>For <code class="docutils literal notranslate"><span class="pre">.csv</span></code> formatted files, the first row should contain the time axis labels, and the first column
should contain the wavelength axis labels.</p>
<p>Example <code class="docutils literal notranslate"><span class="pre">.ufs</span></code> files are available on the web <a class="reference external" href="https://ultrafast.systems/download/surface-xplorer/Data+examples+ufs+format.zip">here</a>.</p>
</section>
<section id="navigation">
<h2>Navigation<a class="headerlink" href="#navigation" title="Link to this heading">¶</a></h2>
<p>Typical time-resolved dataset contains multidimensional data, for example, intensity as a function
of wavelength, time, and scan number. The raw data is plotted in several panels to help view this
through “slices” through the different dimensions.</p>
<figure class="align-default" id="id1">
<a class="reference internal image-reference" href="_images/data-done.png"><img alt="Data panel of the TRSpectrometer software showing a raw data set." src="_images/data-done.png" style="width: 100%;" />
</a>
<figcaption>
<p><span class="caption-text">Data panel of the software showing some raw data.</span><a class="headerlink" href="#id1" title="Link to this image">¶</a></p>
</figcaption>
</figure>
<p>In the example image above, the plot panels are:</p>
<ul class="simple">
<li><p>Upper-left: the complete data set as a “heatmap” image, built using the average of all the
selected scans. This shows the change-in-absorbance (ΔA) signal as a function of wavelength and
pump–probe delay time.</p></li>
<li><p>Upper-right: the “temporal slice”, which plots slices through the data set at one selected
wavelength. The wavelength selection is performed using the green crosshairs in either the
upper-left or lower-left panels. Each scan is plotted in a different colour, and the average of
the selected scans in plotted in yellow.</p></li>
<li><p>Lower-left: the “spectral slice”, which plots slices through the data set at one selected time.
The time selection is performed using the green crosshairs in either the upper-left or upper-right
panels. Each scan is plotted in a different colour, and the average of the selected scans in
plotted in yellow.</p></li>
<li><p>Colour bar: a vertical histogram shows the distribution of the intensity values. The dark blue
selection region chooses the intensity range to use for the colour map. The colour map can be
modified using the arrows to the right of the colour bar, or by right-clicking the colour bar
itself.</p></li>
<li><p>Lower-right: A series of check boxes can be used to select which scans are displayed and used in
the averaging of the data set. The background colour of the check box is the same as its
respective trace in the plot panels. Hovering the mouse over a check box will highlight the
corresponding traces in white. Conversely, clicking on a trace in the temporal or spectral slice
plots will highlight the corresponding check box.</p></li>
</ul>
<figure class="align-default" id="id2">
<a class="reference internal image-reference" href="_images/data-select.png"><img alt="Data panel of the TRSpectrometer software showing selection of raw data traces." src="_images/data-select.png" style="width: 100%;" />
</a>
<figcaption>
<p><span class="caption-text">Selecting scans to be used in averaging of the data set. Note the white highlighting of the
traces which correspond to the check box currently being pointed at with the mouse.</span><a class="headerlink" href="#id2" title="Link to this image">¶</a></p>
</figcaption>
</figure>
<p>Use the mouse to interact with the plot areas:</p>
<ul>
<li><p>Left mouse button:</p>
<blockquote>
<div><ul class="simple">
<li><p>Drag elements such as the crosshairs (green lines).</p></li>
<li><p>Select traces in the temporal or spectral slice panels.</p></li>
<li><p>Drag the plot area to pan along both axes.</p></li>
<li><p>Drag axis labels to pan along only that single axis.</p></li>
</ul>
</div></blockquote>
</li>
<li><p>Right mouse button:</p>
<blockquote>
<div><ul class="simple">
<li><p>Drag the plot areas to zoom in both axes.</p></li>
<li><p>Drag axis labels to zoom in only that single axis.</p></li>
</ul>
</div></blockquote>
</li>
<li><p>Middle mouse button (wheel click):</p>
<blockquote>
<div><ul class="simple">
<li><p>Drag the plot area to pan. As the left button, but ignores elements like the crosshairs.</p></li>
</ul>
</div></blockquote>
</li>
<li><p>Mouse wheel:</p>
<blockquote>
<div><ul class="simple">
<li><p>Scroll on plot areas to zoom in both axes.</p></li>
<li><p>Scroll on axis labels to zoom in only that single axis.</p></li>
</ul>
</div></blockquote>
</li>
</ul>
<figure class="align-default" id="id3">
<a class="reference internal image-reference" href="_images/data-zoom.png"><img alt="Data panel of the TRSpectrometer software showing zooming into a particular region." src="_images/data-zoom.png" style="width: 100%;" />
</a>
<figcaption>
<p><span class="caption-text">View of the raw data when zoomed in to a small section of the data set. This section is around
the time zero.</span><a class="headerlink" href="#id3" title="Link to this image">¶</a></p>
</figcaption>
</figure>
<p>The plot areas are linked, in that a change in the zoom or pan of one plot will be mirrored in the
other panels.</p>
<p>Note that zooming occurs around the location of the mouse pointer. That is, the plot will zoom in to
where the pointer is.</p>
<p>Small 🄰 buttons appear at the lower-left corner of each plot panel. Clicking them will return to
an “auto” zoom mode which will attempt to display the entire data set.</p>
<section id="saving-raw-data">
<h3>Saving Raw Data<a class="headerlink" href="#saving-raw-data" title="Link to this heading">¶</a></h3>
<p>Data can be saved in the application’s native format using the <code class="docutils literal notranslate"><span class="pre">Dataset→Save</span> <span class="pre">As...</span></code> menu item.
Note that the native format is actually a <a class="reference external" href="https://zarr.readthedocs.io">zarr</a> directory tree, and
not an individual file. These data directories will have the <code class="docutils literal notranslate"><span class="pre">.tr.zarr</span></code> suffix appended to their
names.</p>
<p>An average of the selected scans can be saved using the <code class="docutils literal notranslate"><span class="pre">Dataset→Export→Raw</span> <span class="pre">Data</span> <span class="pre">Average...</span></code> menu
item. Both comma-separated values (<code class="docutils literal notranslate"><span class="pre">.csv</span></code>) and the proprietary <code class="docutils literal notranslate"><span class="pre">.ufs</span></code> format from Ultrafast
Systems are supported. These exported data formats are useful for analysis in other software
packages such as <a class="reference external" href="https://glotaran.org/">Glotaran</a> or <a class="reference external" href="https://ultrafastsystems.com/surface-xplorer-data-analysis-software/">Surface Xplorer</a>.</p>
</section>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="Main">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">Loading and Viewing Raw Data</a><ul>
<li><a class="reference internal" href="#loading-raw-data">Loading Raw Data</a></li>
<li><a class="reference internal" href="#navigation">Navigation</a><ul>
<li><a class="reference internal" href="#saving-raw-data">Saving Raw Data</a></li>
</ul>
</li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="acquisition_params.html"
                          title="previous chapter">Configuring Acquisition Parameters</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="dataformat.html"
                          title="next chapter">Native Data Format</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/viewing.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<search id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</search>
<script>document.getElementById('searchbox').style.display = "block"</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="Related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="dataformat.html" title="Native Data Format"
             >next</a> |</li>
        <li class="right" >
          <a href="acquisition_params.html" title="Configuring Acquisition Parameters"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">TRSpectrometer 1.2.4 documentation</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Loading and Viewing Raw Data</a></li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
    &#169; Copyright 2021, Patrick Tapping.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 8.1.3.
    </div>
  </body>
</html>