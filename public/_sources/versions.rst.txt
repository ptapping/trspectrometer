Version History
===============

1.2.4 (2025-02-17)
------------------

- Pin zarr dependency to version 2 to avoid breakage from version 3.

1.2.2 (2022-05-02)
------------------

- Add save functionality to log window.
- Add new Zyla alignment mode for pixel saturation detection.

1.2.0 (2022-02-01)
------------------

- Add laser repetition rate selection menu plugin (:data:`~trspectrometer.plugins.repratemenu`)
- Add Andor Zyla alignment panel plugin (:data:`~trspectrometer.plugins.zylaalign`)
- Consolidate event signals into a dedicated SignalStorage object (:data:`~trspectrometer.signalstorage`)
- Add grid, mouse cursor crosshair and coordinate display to scope panel

1.1.0 (2021-11-14)
------------------

- Support for Python 3.10
- Move from PySide2 to PySide6 for Qt bindings
- Fix excessive newlines appearing in config file


1.0.0 (2021-11-15)
------------------

- Initial release
- Reference transient absorption spectrometer design and hardware support
- Data acquisition, visualisation
- Scope and delay alignment utilities