<!DOCTYPE html>

<html lang="en" data-content_root="./">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Configuring Acquisition Parameters &#8212; TRSpectrometer 1.2.4 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css?v=d75fae25" />
    <link rel="stylesheet" type="text/css" href="_static/sphinxdoc.css?v=34905f61" />
    <link rel="stylesheet" type="text/css" href="_static/mystnb.4510f1fc1dee50b3e5859aac5469c37c29e427902b24a333a5f9fcb2f0b3ac41.css" />
    <link rel="stylesheet" type="text/css" href="_static/css/custom.css?v=b64a1d6a" />
    <script src="_static/documentation_options.js?v=928db92d"></script>
    <script src="_static/doctools.js?v=9bcbadda"></script>
    <script src="_static/sphinx_highlight.js?v=dc90522c"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Loading and Viewing Raw Data" href="viewing.html" />
    <link rel="prev" title="Alignment" href="alignment.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="Related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="viewing.html" title="Loading and Viewing Raw Data"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="alignment.html" title="Alignment"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">TRSpectrometer 1.2.4 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="acquisition.html" accesskey="U">Acquiring Data</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Configuring Acquisition Parameters</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="configuring-acquisition-parameters">
<span id="acquisition-parameters"></span><h1>Configuring Acquisition Parameters<a class="headerlink" href="#configuring-acquisition-parameters" title="Link to this heading">¶</a></h1>
<p>There are no hard rules about what the “correct” acquisition parameters are. The selection is
usually a balance between signal-to-noise versus acquisition time, data resolution versus disk and
memory space (and acquisition time) etc. The general goal is to capture any rapidly changing
dynamics of the sample with sufficient time resolution (such as around time zero), but use reduced
sampling at later times when the sample dynamics evolve more slowly. Some sampling before time zero
is also desirable to evaluate the level of background signal from pump scattering or spontaneous
emission etc.</p>
<p>Here, we will show an example of how some acquisition parameters might be chosen. Remember however,
this is just an example and you should use your own judgement about what parameters will meet your
needs and are physically possible on the equipment.</p>
<figure class="align-default" id="id1">
<img alt="Data panel of the TRSpectrometer software showing configuration of data acquisition parameters." src="_images/data-setup.png" />
<figcaption>
<p><span class="caption-text">Configuration section of the Data panel with example parameters entered.</span><a class="headerlink" href="#id1" title="Link to this image">¶</a></p>
</figcaption>
</figure>
<section id="time-section">
<h2>Time Section<a class="headerlink" href="#time-section" title="Link to this heading">¶</a></h2>
<p>After completing the <a class="reference internal" href="alignment.html#alignment"><span class="std std-ref">Alignment</span></a> process, the “time zero” should be known. This is the absolute
delay time when the pump and probe beams arrive on the sample simultaneously and the signal is just
beginning to appear. For this example, the time zero was found to be at 117.5 ps. We decide to
sample 25 ps of background. We also want a little bit of leeway before the signal begins to appear,
so we set the <code class="docutils literal notranslate"><span class="pre">Start</span></code> parameter to 26 ps before time zero, or 91.5 ps.</p>
<p>We choose a <code class="docutils literal notranslate"><span class="pre">Step</span> <span class="pre">Type</span></code> of <code class="docutils literal notranslate"><span class="pre">Variable</span></code>, since we want to be able to have better time resolution
around time zero than the bulk of the data set. Choosing the alternative <code class="docutils literal notranslate"><span class="pre">Fixed</span></code> step mode with a
large step size may be appropriate for initial scans to quickly get an idea of the sample behaviour.</p>
<p>For a variable step type, the <code class="docutils literal notranslate"><span class="pre">Variable</span> <span class="pre">Steps</span></code> table needs to be filled in. Each row in the table
represents a time step range. The columns are the number of steps (or data points) for the range,
the time window of the range, and the step size (resolution) of the range. Only the second and third
columns are editable, while the number of steps will be computed from the window size and
resolution. Use the <code class="docutils literal notranslate"><span class="pre">+</span></code> and <code class="docutils literal notranslate"><span class="pre">-</span></code> buttons below the table to add rows to the table, or remove the
currently selected row.</p>
<p>We already decided we wanted 25 ps of background data before time zero, so the first row in the
table has a window of 25 ps. A resolution of 0.5 ps is selected to give 50 time points in this
region. This is quite generous, but a good rule is to collect at least 10 data points here.</p>
<p>The second step range should capture the rapid dynamics of the system around time zero. The window
needs to be wide enough to see the signal rise across the full spectral window, taking into
consideration the instrument response time and chirp (temporal dispersion) in the probe light, as
well as any rapid changes in the sample directly after excitation. As the resolution needs to be
quite high over this region, it’s also desirable to have this window be the smallest possible to
avoid collecting an excessive amount of data in this region. We choose a 10 ps window with a
resolution of 100 fs to give 100 data points in this region.</p>
<p>The subsequent step ranges are somewhat arbitrarily chosen so that the next 100 ps of data are
collected with 1 ps resolution followed by 1 ns at 10 ps resolution, giving 100 data points in each
of those ranges. Finally, we know this particular sample has a long-lived species present, so we run
the final step range out to the limits of what is possible with the installed delay hardware with a
resolution of 25 ps. The selected window of 6.8 ns is enough to exceed the limits of the delay by 21
ps, indicated by the red highlight and <code class="docutils literal notranslate"><span class="pre">(+21</span> <span class="pre">ps)</span></code> in the totals displayed below the table.</p>
<p>The numbers directly below the table provide the sum total of the sampling window, as well as the
total number of time points. The number of points will have a direct impact on both the data
acquisition time, and storage space required. If the sampling window will exceed the capabilities of
the delay hardware (also taking into consideration the start time), this display will be highlighted
in red. The window will be truncated automatically during the acquisition process. The excess time
window is given in parenthesis. In this example, the +21 ps excess will cause the final delay range
to actually be 6779 ps instead of the requested 6800 ps.</p>
</section>
<section id="scans-section">
<h2>Scans Section<a class="headerlink" href="#scans-section" title="Link to this heading">¶</a></h2>
<p>The <code class="docutils literal notranslate"><span class="pre">Count</span></code> value determines how many repeats of the acquisition process should be performed. The
individual scans will be averaged which helps improve signal-to-noise, particularly compensating for
longer-term fluctuations such as pump laser power drift, slight alignment changes due to air
conditioning cycles or similar. It will also help smooth over “blips” which may be caused by
momentary scattering from particles in a solution sample, or accidental bumps on the equipment. The
number of scans can be adjusted during the acquisition process, so it can be good to initially set
this value much higher than expected, then dial it down to stop the acquisition process when the
quality of data begins looking good enough.</p>
<p>The <code class="docutils literal notranslate"><span class="pre">Method</span></code> selects the strategy used to acquire the data. The available choices will depend on
the <a class="reference internal" href="api/trspectrometer.plugins.acquisition.html#module-trspectrometer.plugins.acquisition" title="trspectrometer.plugins.acquisition"><code class="xref py py-data docutils literal notranslate"><span class="pre">acquisition</span></code></a> <a class="reference internal" href="configuration.html#plugins"><span class="std std-ref">Plugins</span></a> which are loaded and their particular
<a class="reference internal" href="configuration.html#configuration"><span class="std std-ref">Configuration</span></a>. We have selected <code class="docutils literal notranslate"><span class="pre">Swept</span> <span class="pre">Acquisition</span></code> here. The <a class="reference internal" href="api/trspectrometer.plugins.acquisition.ta_swept.html#module-trspectrometer.plugins.acquisition.ta_swept" title="trspectrometer.plugins.acquisition.ta_swept"><code class="xref py py-data docutils literal notranslate"><span class="pre">swept</span> <span class="pre">acquisition</span></code></a> is highly recommended if the hardware is
capable of this mode. The more traditional <a class="reference internal" href="api/trspectrometer.plugins.acquisition.ta_stepped.html#module-trspectrometer.plugins.acquisition.ta_stepped" title="trspectrometer.plugins.acquisition.ta_stepped"><code class="xref py py-data docutils literal notranslate"><span class="pre">stepped</span> <span class="pre">acquisition</span></code></a> should be available to all hardware types
but will be much slower for the same data quality.</p>
<p>The <code class="docutils literal notranslate"><span class="pre">Density</span></code> parameter selects the desired number of samples performed at each time step of each
scan. In this example, a value of 100 means to take 100 ΔA measurements (200 laser shots) and
average them for each time point. For a swept acquisition method, this determines how fast the delay
is swept and has an approximately linear proportionality to the acquisition time. In this case it is
suggested to use a relatively low density (50 to 200 per step) but a large (10 to 50) number of
scans. For a stepped acquisition method, significant amounts of time are spent moving the delay into
position (and not actually sampling data), therefore it is optimal to do more sampling at each step
(200 to 500 per step), but a smaller number of scans will be possible (3 to 10) within a reasonable
time frame.</p>
</section>
<section id="metadata-section">
<h2>Metadata Section<a class="headerlink" href="#metadata-section" title="Link to this heading">¶</a></h2>
<p>Metadata is “data about the data”. These fields are completely optional, but worth filling out for
the benefit of any future users of the data, which are likely to be you!</p>
<p>The <code class="docutils literal notranslate"><span class="pre">Sample</span></code> field can be used for the name of the sample. It will be used as part of the
suggested file name when the data is saved.</p>
<p>The <code class="docutils literal notranslate"><span class="pre">Pump</span></code> field should be used to describe the nature of the pump laser, such as its wavelength,
pulse energy, and polarisation. It will also be used as part of the suggested file name when the
data is saved.</p>
<p><code class="docutils literal notranslate"><span class="pre">Operator</span></code> should be the name(s) of the people collecting the data.</p>
<p><code class="docutils literal notranslate"><span class="pre">Note</span></code> can be any additional information about the sample, equipment, conditions etc which might
be relevant.</p>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="Main">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">Configuring Acquisition Parameters</a><ul>
<li><a class="reference internal" href="#time-section">Time Section</a></li>
<li><a class="reference internal" href="#scans-section">Scans Section</a></li>
<li><a class="reference internal" href="#metadata-section">Metadata Section</a></li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="alignment.html"
                          title="previous chapter">Alignment</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="viewing.html"
                          title="next chapter">Loading and Viewing Raw Data</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/acquisition_params.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<search id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</search>
<script>document.getElementById('searchbox').style.display = "block"</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="Related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="viewing.html" title="Loading and Viewing Raw Data"
             >next</a> |</li>
        <li class="right" >
          <a href="alignment.html" title="Alignment"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">TRSpectrometer 1.2.4 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="acquisition.html" >Acquiring Data</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Configuring Acquisition Parameters</a></li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
    &#169; Copyright 2021, Patrick Tapping.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 8.1.3.
    </div>
  </body>
</html>