#!/usr/bin/env python3

if __name__ == "__main__":
    __package__ = "trspectrometer"
    import trspectrometer.__main__
    trspectrometer.__main__.main()