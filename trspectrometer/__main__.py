#!/usr/bin/env python3

from . import mainwindow

def main():
    mainwindow.main()

if __name__ == "__main__":
    main()
