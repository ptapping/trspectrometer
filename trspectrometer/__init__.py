"""
Software to run a time-resolved (TR) spectrometer.
"""

__version__ = "1.2.4"

def main():
    """
    Run the :meth:`__main__.main` method, which launches the main window of the application.
    """
    from . import __main__
    __main__.main()
